$(document).ready(function () {

  console.log("main scripts");

  $(window).on('load', function () {
    $('#myModal').modal('show');
  });



//add param
  $('body').on('click', '.addRule', function () {

    let closeMultipleRow = $(this).closest(".multipleRow");
    let getHtml = $(this).closest(".customRule").html()

    closeMultipleRow.append('<div class="customRule input-group form-group mb-2"> ' + getHtml + '</div>')
    closeMultipleRow.find(".customRule:first .btn").removeClass("addRule colorBlue").addClass("removeRule colorRed").text("remove rule");
  });

//remove param
  $('body').on('click', '.removeRule', function () {

    let closeMultipleRow = $(this).closest(".multipleRow");
    let checkLength = closeMultipleRow.find(".addRule").length;

    if (checkLength == 1) {
      closeMultipleRow.find(".customRule:first").remove();
      closeMultipleRow.find(".customRule:first .btn").removeClass("removeRule colorRed").addClass("addRule colorBlue").text("add rule")
    } else {
      closeMultipleRow.find(".customRule:eq(1) .btn").addClass("removeRule colorRed").removeClass("addRule colorBlue").text("remove rule")
      $(this).closest(".customRule").remove();
    }

  });



  checkRulesRow();
  let ruleContainerHtml = $(".ruleContainer").html();
  let number = 1;
  
  //add new rule row
  $('body').on('click', '.iconPlus', function () {

    let getHtml = ruleContainerHtml;
    number++
    let addRuleClass = "addRuleContainer" + number
    $(".customRuleWrapper").append('  <div class="' + addRuleClass + ' ruleContainer row background-light-grey border-top">' + getHtml + '</div>')
    $(this).hide();
    checkRulesRow();
    updateRuleNumber();

  });

//remove rule rolw
  $('body').on('click', '.iconMinus', function () {
    $(this).closest(".ruleContainer").remove();
    checkRulesRow();
    updateRuleNumber();
  });
  
  //check state of rule row
  function checkRulesRow() {
    let ruleRows = $(".customRuleWrapper").find(".iconMinus").length
    // console.log(ruleRows)
    if (ruleRows == 1 ) {
      $('.iconMinus').click(false);
      $(".iconPlus").show();
    } else {
       $(".iconPlus:last").show();
    }
  }

  function updateRuleNumber() {
    var new_number = 1;
    $('.ruleContainer h6').each(function () {
      $(this).text("Rule " + new_number);
      new_number++;
    });
  }




});
//ready end
